﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WCFTest.aspx.cs" Inherits="PlanoRendasWCFTester.WCFTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Plano de Rendas WCF - Tester</title>
</head>
<body style="font-family: Tahoma,Arial; font-size: 9pt;">
    <form id="form1" runat="server">
        <fieldset>
            <legend>Filtros:</legend>
            <table border="0">
                <tr>
                    <td><b>Método:</b></td>
                    <td>
                        <asp:DropDownList ID="ddlMetodo" runat="server">
                            <asp:ListItem Selected="True" Value="GetPlan">Plano de Rendas</asp:ListItem>
                            <asp:ListItem Value="GetTAE">TAE (TAEG)</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Button ID="btnProcessar" runat="server" Text="Processar" OnClick="btnProcessar_Click" />
        </fieldset>
        <br />
        <br />
        <div id="divResultado" runat="server" style="font-family: Tahoma,Arial,Verdana; font-size: 9pt;">
        </div>
        <div id="divResultado2" runat="server">
            <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" BorderStyle="None" OnRowDataBound="GridView1_RowDataBound">
                <AlternatingRowStyle BackColor="White" />
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <RowStyle BackColor="#F7F7DE" />
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True" />
                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                <SortedAscendingHeaderStyle BackColor="#848384" />
                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                <SortedDescendingHeaderStyle BackColor="#575357" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
