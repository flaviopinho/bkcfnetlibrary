﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using PlanoRendasWCFTester.PlanoDeRendasWCF;

namespace PlanoRendasWCFTester
{
    public partial class WCFTest : System.Web.UI.Page
    {

        #region Variáveis Globais
        //Variáveis para o GetPlan
        private double _Amount; //Valor total financiado
        private int _Num_Period; //Número total de períodos
        private char _Periodicity; //M -> Mensal | T -> Trimestral | S -> Semestral | A -> Anual
        private double _Stamp; //Imposto de Selo
        private double _StampTax; //Taxa de Imposto de Selo
        private double[][] _TAN; //TAN com a respectiva taxa por período

        //Variáveis para o GetTAE
        private double[] _CashFlows;
        #endregion 

        #region Main - Definir valores de entrada para o métodos
        protected void Page_Load(object sender, EventArgs e)
        {

            //Parâmetros para o método GetPlan
            _Amount = 5378.25; //Valor total financiado
            _Num_Period = 6; //Número total de períodos
            _Periodicity = 'S'; //M -> Mensal | T -> Trimestral | S -> Semestral | A -> Anual
            _Stamp = 72.62; //Imposto de Selo
            _StampTax = 4.00; //Taxa de Imposto de Selo
            _TAN = new double[][] { //TAN com a respectiva taxa por período
                                              new double[] {1, 9.750},
                                              new double[] {2, 9.750},
                                              new double[] {3, 9.750},
                                              new double[] {4, 9.750},
                                              new double[] {5, 9.750},
                                              new double[] {6, 9.750},
                                             };


            //Parâmetros para o método GetTAE
            _CashFlows = new double[] { -5149.64,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         173.89,
                                         174.03
                                    };
        }
        #endregion

        #region Funções e Eventos
        protected void btnProcessar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlMetodo.SelectedValue == @"GetPlan")
                {
                    //Obtém Plano de Rendas
                    GetPlan(_Amount, _Num_Period, _Periodicity, _Stamp, _StampTax, _TAN);
                }
                else if (ddlMetodo.SelectedValue == @"GetTAE")
                {
                    //Obtém TAE (TAEG)
                    GetTAE(_CashFlows);
                }
            }
            catch (Exception Ex)
            {
                divResultado.InnerHtml = "<span style='color: red;'><b>Erro:</b>" + Ex.Message.ToString() + "<br><br>";
                GridView1.Visible = false;
            }
        }


        protected void GetPlan(double _Amount, int _Num_Period, char _Periodicity, double _Stamp,
                                double _StampTax, double[][] _TAN)
        {

            divResultado.InnerHtml = "<b>Resultado:</b><br><br>";
            GridView1.Visible = false;

            try
            {

                PlanoRendasClient myWS = new PlanoRendasClient();
                myWS.InnerChannel.OperationTimeout = new TimeSpan(0, 5, 0);
                DataTable dt = myWS.GetPlan(_Amount, _TAN, _Num_Period, _Periodicity, _Stamp, _StampTax);

                if (dt != null && dt.Rows.Count > 0)
                {

                    GridView1.Visible = true;
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    divResultado.InnerHtml += "<span style='color: gray;'>Sem resultados</span>";
                }

            }
            catch (Exception ex)
            {
                //divResultado.InnerHtml += "<span style='color: gray;'>" + ex.Message.ToString() + "</span>";
                divResultado.InnerHtml += string.Format("<span style='color: red;'>{0}</span>", ex.Message.ToString());
            }

        }


        protected void GetTAE(double[] _CashFlows)
        {

            divResultado.InnerHtml = "<b>Resultado:</b><br><br>";
            GridView1.Visible = false;

            try
            {
                PlanoRendasClient myWS = new PlanoRendasClient();
                myWS.InnerChannel.OperationTimeout = new TimeSpan(0, 5, 0);
                double? TAE = myWS.GetTAE(_CashFlows);

                if (!string.IsNullOrEmpty(TAE.ToString()))
                {
                    divResultado.InnerHtml += "<b>TAE: </b>" + TAE;
                }
                else
                {
                    divResultado.InnerHtml += "<span style='color: gray;'>Sem resultados</span>";
                }

            }
            catch (Exception ex)
            {
                //divResultado.InnerHtml += "<span style='color: gray;'>" + ex.Message.ToString() + "</span>";
                divResultado.InnerHtml += string.Format("<span style='color: red;'>{0}</span>", ex.Message.ToString());
            }

        }


        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Text = "Tx Juros (%)";
                e.Row.Cells[2].Text = "Amortização (€)";
                e.Row.Cells[3].Text = "Juros (€)";
                e.Row.Cells[4].Text = "Prestação (€)";
                e.Row.Cells[5].Text = "Capital em Dívida (€)";
            } else if(e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex == 0)
            {
                e.Row.Cells[0].Text = "";
                e.Row.Cells[1].Text = "";
                e.Row.Cells[2].Text = "";
                e.Row.Cells[3].Text = "";
                e.Row.Cells[4].Style.Add("font-weight", "bold");
                e.Row.Cells[5].Style.Add("font-weight", "bold");
            }
        }

        #endregion

    }
}